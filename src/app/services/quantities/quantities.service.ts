import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class QuantitiesService {

  baconQty:number;
  cheeseQty:number;
  lettuceQty:number;
  meatQty:number;
  onionQty:number;
  pickleQty:number;
  tomatoQty:number;

  constructor() {
    this.initQtys();
   }

   initQtys(){
    this.baconQty = 0;
    this.cheeseQty = 0;
    this.lettuceQty = 0;
    this.meatQty = 0;
    this.onionQty = 0;
    this.pickleQty = 0;
    this.tomatoQty = 0;
   }
}
