import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PricesService {

  baconPrice:number;
  cheesePrice:number;
  lettucePrice:number;
  meatPrice:number;
  onionPrice:number;
  picklePrice:number;
  tomatoPrice:number;

  constructor() { 
    this.initPrices();
  }
initPrices(){
  this.baconPrice = 0;
  this.cheesePrice = 0;
  this.lettucePrice = 0;
  this.meatPrice = 0;
  this.onionPrice = 0;
  this.picklePrice = 0;
  this.tomatoPrice = 0;
}
}
