import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ingredient } from 'src/app/interfaces/ingredient/ingredient.interface';
@Injectable({
  providedIn: 'root',
})
export class IngredientsService {
  constructor(private http: HttpClient) {}

  getAllIngredients() {
    return this.http.get<Ingredient[]>('http://localhost:3000/ingredients');
  }
}
