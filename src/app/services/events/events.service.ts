import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  addBaconFn = new EventEmitter();
  addCheeseFn = new EventEmitter();
  addLettuceFn = new EventEmitter();
  addMeatFn = new EventEmitter();
  addOnionFn = new EventEmitter();
  addPickleFn = new EventEmitter();
  addTomatoFn = new EventEmitter();

  delBaconFn = new EventEmitter();
  delCheeseFn = new EventEmitter();
  delLettuceFn = new EventEmitter();
  delMeatFn = new EventEmitter();
  delOnionFn = new EventEmitter();
  delPickleFn = new EventEmitter();
  delTomatoFn = new EventEmitter();

  assembleOrderFn = new EventEmitter();
  updateGlobalTotalFn = new EventEmitter();

  resetBurgerFn = new EventEmitter();

  subsVar: Subscription;

  constructor() {}

  baconAddFn_exec = () => { this.addBaconFn.emit() };
  cheeseAddFn_exec = () => { this.addCheeseFn.emit() };
  lettuceAddFn_exec = () => { this.addLettuceFn.emit() };
  meatAddFn_exec = () => { this.addMeatFn.emit() };
  onionAddFn_exec = () => { this.addOnionFn.emit() };
  pickleAddFn_exec = () => { this.addPickleFn.emit() };
  tomatoAddFn_exec = () => { this.addTomatoFn.emit() };

  baconDelFn_exec = () => { this.delBaconFn.emit() };
  cheeseDelFn_exec = () => { this.delCheeseFn.emit() };
  lettuceDelFn_exec = () => { this.delLettuceFn.emit() };
  meatDelFn_exec = () => { this.delMeatFn.emit() };
  onionDelFn_exec = () => { this.delOnionFn.emit() };
  pickleDelFn_exec = () => { this.delPickleFn.emit() };
  tomatoDelFn_exec = () => { this.delTomatoFn.emit() };

  assembleOrderFn_exec = () => { this.assembleOrderFn.emit() };
  updateGlobalTotalFn_exec = () => { this.updateGlobalTotalFn.emit() };

  resetBurgerFn_exec = () => {this.resetBurgerFn.emit()};
}
