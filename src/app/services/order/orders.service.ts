import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {IngredientDetail} from '../../interfaces/ingredient/ingredient-detail.interface';
import {BurgerRequest } from 'src/app/interfaces/burger/burger-request.interface';
import {BurgerResponse } from 'src/app/interfaces/burger/burger-response.interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private http:HttpClient) { }
  
  addOrder(order:IngredientDetail[]){
    
    let burger: BurgerRequest = {"ingredients": order};
    let jsonedBurger = JSON.stringify(burger);

    this.http.post<BurgerRequest>('http://localhost:3000/burgers',jsonedBurger,httpOptions).subscribe(
      res => {
        alert('Your burguer has been saved...')
      }
    );
  }

  getOrders(){
    return this.http.get<BurgerResponse[]>('http://localhost:3000/burgers');
  }
}
