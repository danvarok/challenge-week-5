import { IngredientDetail } from "../ingredient/ingredient-detail.interface";

export interface BurgerResponse{
  id:number;
  ingredients:IngredientDetail[];
}