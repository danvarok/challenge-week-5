import { IngredientDetail } from "../ingredient/ingredient-detail.interface";

export interface BurgerRequest{
  ingredients:IngredientDetail[];
}