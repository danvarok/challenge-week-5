export interface IngredientDetail{
  name:string;
  price:number;
  qty:number;
  total:number;
}