import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BurgerComponent } from './components/burger/burger.component';
import { HistoryThumbnailComponent } from './components/history/history-thumbnail/history-thumbnail.component';
import { HistoryComponent } from './components/history/history.component';
import { BaconComponent } from './components/ingredients/bacon/bacon.component';
import { BunBottomComponent } from './components/ingredients/bun-bottom/bun-bottom.component';
import { BunTopComponent } from './components/ingredients/bun-top/bun-top.component';
import { CheeseComponent } from './components/ingredients/cheese/cheese.component';
import { AddButtonComponent } from './components/ingredients/ingredients-menu/add-button/add-button.component';
import { DelButtonComponent } from './components/ingredients/ingredients-menu/del-button/del-button.component';
import { IngredientsMenuComponent } from './components/ingredients/ingredients-menu/ingredients-menu.component';
import { LettuceComponent } from './components/ingredients/lettuce/lettuce.component';
import { MeatComponent } from './components/ingredients/meat/meat.component';
import { OnionComponent } from './components/ingredients/onion/onion.component';
import { PickleComponent } from './components/ingredients/pickle/pickle.component';
import { TomatoComponent } from './components/ingredients/tomato/tomato.component';
import { OrderComponent } from './components/order/order.component';
import { CurrencyPipe } from './pipes/currency.pipe';
import { EventsService } from './services/events/events.service';
import { IngredientsService } from './services/ingredients/ingredients.service';
import { OrdersService } from './services/order/orders.service';
import { QuantitiesService } from './services/quantities/quantities.service';


@NgModule({
  declarations: [
    AppComponent,
    OrderComponent,
    BunBottomComponent,
    LettuceComponent,
    MeatComponent,
    CheeseComponent,
    BaconComponent,
    TomatoComponent,
    PickleComponent,
    IngredientsMenuComponent,
    BurgerComponent,
    OnionComponent,
    BunTopComponent,
    CurrencyPipe,
    AddButtonComponent,
    DelButtonComponent,
    HistoryComponent,
    HistoryThumbnailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [IngredientsService,EventsService,QuantitiesService,OrdersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
