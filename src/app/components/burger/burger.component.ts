import {
  Component,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
} from '@angular/core';
import { EventsService } from 'src/app/services/events/events.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';
import { BaconComponent } from '../ingredients/bacon/bacon.component';
import { CheeseComponent } from '../ingredients/cheese/cheese.component';
import { LettuceComponent } from '../ingredients/lettuce/lettuce.component';
import { MeatComponent } from '../ingredients/meat/meat.component';
import { OnionComponent } from '../ingredients/onion/onion.component';
import { PickleComponent } from '../ingredients/pickle/pickle.component';
import { TomatoComponent } from '../ingredients/tomato/tomato.component';

@Component({
  selector: 'app-burger',
  templateUrl: './burger.component.html',
  styleUrls: ['./burger.component.css'],
})
export class BurgerComponent {
  @ViewChild('componentcontainer', { read: ViewContainerRef })
  entry: ViewContainerRef;
  baconInstances: ComponentRef<any>[] = [];
  cheeseInstaces: ComponentRef<any>[] = [];
  lettuceInstances: ComponentRef<any>[] = [];
  meatInstances: ComponentRef<any>[] = [];
  onionInstances: ComponentRef<any>[] = [];
  pickleInstances: ComponentRef<any>[] = [];
  tomatoInstances: ComponentRef<any>[] = [];

  constructor(
    private resolver: ComponentFactoryResolver,
    private eventService: EventsService,
    private qtyService: QuantitiesService
  ) {
  }

  ngOnInit(): void {
    this.subscribeCreateFn();
    this.subscribeDeleteFn();
    this.subscribeResetFn();
  }

  subscribeCreateFn() {
    if (this.eventService.subsVar == undefined) {
      this.eventService.subsVar = this.eventService.addBaconFn.subscribe(
        (name: string) => {
          this.createBaconComponent(this.entry);
        }
      );
      this.eventService.subsVar = this.eventService.addCheeseFn.subscribe(
        (name: string) => {
          this.createCheeseComponent(this.entry);
        }
      );
      this.eventService.subsVar = this.eventService.addLettuceFn.subscribe(
        (name: string) => {
          this.createLettuceComponent(this.entry);
        }
      );
      this.eventService.subsVar = this.eventService.addMeatFn.subscribe(
        (name: string) => {
          this.createMeatComponent(this.entry);
        }
      );
      this.eventService.subsVar = this.eventService.addOnionFn.subscribe(
        (name: string) => {
          this.createOnionComponent(this.entry);
        }
      );
      this.eventService.subsVar = this.eventService.addPickleFn.subscribe(
        (name: string) => {
          this.createPickleComponent(this.entry);
        }
      );
      this.eventService.subsVar = this.eventService.addTomatoFn.subscribe(
        (name: string) => {
          this.createTomatoComponent(this.entry);
        }
      );
    }
  }

  subscribeDeleteFn() {
    this.eventService.subsVar = this.eventService.delBaconFn.subscribe(
      (name: string) => {
        this.deleteBaconComponent();
      }
    );
    this.eventService.subsVar = this.eventService.delCheeseFn.subscribe(
      (name: string) => {
        this.deleteCheeseComponent();
      }
    );
    this.eventService.subsVar = this.eventService.delLettuceFn.subscribe(
      (name: string) => {
        this.deleteLettuceComponent();
      }
    );
    this.eventService.subsVar = this.eventService.delMeatFn.subscribe(
      (name: string) => {
        this.deleteMeatComponent();
      }
    );

    this.eventService.subsVar = this.eventService.delOnionFn.subscribe(
      (name: string) => {
        this.deleteOnionComponent();
      }
    );
    this.eventService.subsVar = this.eventService.delPickleFn.subscribe(
      (name: string) => {
        this.deletePickleComponent();
      }
    );
    this.eventService.subsVar = this.eventService.delTomatoFn.subscribe(
      (name: string) => {
        this.deleteTomatoComponent();
      }
    );
  }

  subscribeResetFn(){
    this.eventService.subsVar = this.eventService.resetBurgerFn.subscribe(
      (name: string) => {
        this.resetBurger();
      }
    );
  }

  createBaconComponent(entry:ViewContainerRef) {
    const factory = this.resolver.resolveComponentFactory(BaconComponent);
    let newBacon = entry.createComponent(factory);
    this.baconInstances.push(newBacon);
    entry.move(newBacon.hostView, 0);
    newBacon.instance.increaseQty();
    this.updateOrder();
  }

  createCheeseComponent(entry:ViewContainerRef) {
    const factory = this.resolver.resolveComponentFactory(CheeseComponent);
    let newCheese = entry.createComponent(factory);
    this.cheeseInstaces.push(newCheese);
    entry.move(newCheese.hostView, 0);
    newCheese.instance.increaseQty();
    this.updateOrder();
  }

  createLettuceComponent(entry:ViewContainerRef) {
    const factory = this.resolver.resolveComponentFactory(LettuceComponent);
    let newLettuce = entry.createComponent(factory);
    this.entry.move(newLettuce.hostView, 0);
    this.lettuceInstances.push(newLettuce);
    newLettuce.instance.increaseQty();
    this.updateOrder();
  }

  createMeatComponent(entry:ViewContainerRef) {
    const factory = this.resolver.resolveComponentFactory(MeatComponent);
    let newMeat = entry.createComponent(factory);
    this.meatInstances.push(newMeat);
    entry.move(newMeat.hostView, 0);
    newMeat.instance.increaseQty();
    this.updateOrder();
  }

  createOnionComponent(entry:ViewContainerRef) {
    const factory = this.resolver.resolveComponentFactory(OnionComponent);
    let newOnion = entry.createComponent(factory);
    this.onionInstances.push(newOnion);
    entry.move(newOnion.hostView, 0);
    newOnion.instance.increaseQty();
    this.updateOrder();
  }

  createPickleComponent(entry:ViewContainerRef) {
    const factory = this.resolver.resolveComponentFactory(PickleComponent);
    let newPickle = entry.createComponent(factory);
    this.pickleInstances.push(newPickle);
    entry.move(newPickle.hostView, 0);
    newPickle.instance.increaseQty();
    this.updateOrder();
  }

  createTomatoComponent(entry:ViewContainerRef) {
    const factory = this.resolver.resolveComponentFactory(TomatoComponent);
    let newTomato = entry.createComponent(factory);
    this.tomatoInstances.push(newTomato);
    entry.move(newTomato.hostView, 0);
    newTomato.instance.increaseQty();
    this.updateOrder();
  }

  deleteBaconComponent() {
    if (this.baconInstances.length != 0) {
      let lastBacon = this.baconInstances.pop();
      lastBacon?.instance.decreaseQty();
      lastBacon?.destroy();
      this.updateOrder();
    }
  }

  deleteCheeseComponent() {
    if (this.cheeseInstaces.length != 0) {
      let lastCheese = this.cheeseInstaces.pop();
      lastCheese?.instance.decreaseQty();
      lastCheese?.destroy();
      this.updateOrder();
    }
  }

  deleteLettuceComponent() {
    if (this.lettuceInstances.length != 0) {
      let lastLettuce = this.lettuceInstances.pop();
      lastLettuce?.instance.decreaseQty();
      lastLettuce?.destroy();
      this.updateOrder();
    }
  }

  deleteMeatComponent() {
    if (this.meatInstances.length != 0) {
      let lastMeat = this.meatInstances.pop();
      lastMeat?.instance.decreaseQty();
      lastMeat?.destroy();
      this.updateOrder();
    }
  }

  deleteOnionComponent() {
    if (this.onionInstances.length != 0) {
      let lastOnion = this.onionInstances.pop();
      lastOnion?.instance.decreaseQty();
      lastOnion?.destroy();
      this.updateOrder();
    }
  }

  deletePickleComponent() {
    if (this.pickleInstances.length != 0) {
      let lastOnion = this.pickleInstances.pop();
      lastOnion?.instance.decreaseQty();
      lastOnion?.destroy();
      this.updateOrder();
    }
  }

  deleteTomatoComponent() {
    if (this.tomatoInstances.length != 0) {
      let lastTomato = this.tomatoInstances.pop();
      lastTomato?.instance.decreaseQty();
      lastTomato?.destroy();
      this.updateOrder();
    }
  }

  updateOrder() {
    this.eventService.assembleOrderFn_exec();
    this.eventService.updateGlobalTotalFn_exec();
  }

  resetBurger() {
    this.entry.clear();
    this.qtyService.initQtys();
  }
}
