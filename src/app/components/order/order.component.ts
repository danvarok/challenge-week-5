import { Component, OnInit } from '@angular/core';
import { CurrencyService } from 'src/app/services/currency/currency.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';
import { PricesService } from 'src/app/services/prices/prices.service';
import { IngredientDetail } from 'src/app/interfaces/ingredient/ingredient-detail.interface';
import { EventsService } from 'src/app/services/events/events.service';
import { OrdersService } from 'src/app/services/order/orders.service';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
})
export class OrderComponent implements OnInit {
  globalTotal: number;
  orders: IngredientDetail[] = [];

  AUD: boolean;
  USD: boolean;
  MXN: boolean;

  constructor(
    public currencyService: CurrencyService,
    private qtyService: QuantitiesService,
    private pricesService: PricesService,
    private eventsService: EventsService,
    private ordersService:OrdersService
  ) {
    this.globalTotal = 1;
    this.AUD = false;
    this.USD = true;
    this.MXN = false;
  }

  ngOnInit(): void {
    this.subscribeFns();
  }

  subscribeFns() {
    this.eventsService.subsVar = this.eventsService.assembleOrderFn.subscribe(
      (name: string) => {
        this.assembleOrder();
      }
    );

    this.eventsService.subsVar =
      this.eventsService.updateGlobalTotalFn.subscribe((name: string) => {
        this.updateGlobalTotal();
      });
  }

  updateCurrency(e: any) {
    let selectedIndex = e.target.options.selectedIndex;
    let options = e.target.options;
    this.currencyService.currencyCode = options[selectedIndex].value;
    switch (this.currencyService.currencyCode) {
      case 'USD':
        this.USD = true;
        this.AUD = false;
        this.MXN = false;
        break;
      case 'AUD':
        this.AUD = true;
        this.USD = false;
        this.MXN = false;
        break;
      case 'MXN':
        this.MXN = true;
        this.USD = false;
        this.AUD = false;
        break;
      default:
        this.USD = true;
        this.AUD = false;
        this.MXN = false;
        break;
    }
  }

  assembleOrder() {
    this.orders = [];
    this.orders.push({
      name: 'Bacon',
      price: this.pricesService.baconPrice,
      qty: this.qtyService.baconQty,
      total: this.pricesService.baconPrice * this.qtyService.baconQty,
    });

    this.orders.push({
      name: 'Cheese',
      price: this.pricesService.cheesePrice,
      qty: this.qtyService.cheeseQty,
      total: this.pricesService.cheesePrice * this.qtyService.cheeseQty,
    });

    this.orders.push({
      name: 'Lettuce',
      price: this.pricesService.lettucePrice,
      qty: this.qtyService.lettuceQty,
      total: this.pricesService.lettucePrice * this.qtyService.lettuceQty,
    });

    this.orders.push({
      name: 'Meat',
      price: this.pricesService.meatPrice,
      qty: this.qtyService.meatQty,
      total: this.pricesService.meatPrice * this.qtyService.meatQty,
    });

    this.orders.push({
      name: 'Pickle',
      price: this.pricesService.picklePrice,
      qty: this.qtyService.pickleQty,
      total: this.pricesService.picklePrice * this.qtyService.pickleQty,
    });

    this.orders.push({
      name: 'Tomato',
      price: this.pricesService.tomatoPrice,
      qty: this.qtyService.tomatoQty,
      total: this.pricesService.tomatoPrice * this.qtyService.tomatoQty,
    });

    this.orders.push({
      name: 'Onion',
      price: this.pricesService.onionPrice,
      qty: this.qtyService.onionQty,
      total: this.pricesService.onionPrice * this.qtyService.onionQty,
    });

    this.orders = this.orders.filter(function (ingredient) {
      return ingredient.qty > 0;
    });
  }

  updateGlobalTotal() {
    this.globalTotal = 1;
    this.orders.forEach((ingredient) => {
      this.globalTotal += ingredient.total;
    });
  }

  placeOrder() {
    this.ordersService.addOrder(this.orders);
    this.resetOrder();
  }
  
  resetOrder(){
    this.eventsService.resetBurgerFn_exec();
    this.orders = [];
    this.updateGlobalTotal();
  }
}
