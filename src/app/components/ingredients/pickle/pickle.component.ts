import { Component, OnInit } from '@angular/core';
import { PricesService } from 'src/app/services/prices/prices.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-pickle',
  templateUrl: './pickle.component.html',
  styleUrls: ['./pickle.component.css'],
})
export class PickleComponent implements OnInit {
  price: number;
  constructor(private qtyService: QuantitiesService,
    private pricesService: PricesService) {
    this.price = 0.4;
    this.pricesService.picklePrice = this.price;
  }

  ngOnInit(): void {}
  increaseQty() {
    this.qtyService.pickleQty++;
  }

  decreaseQty() {
    if (this.qtyService.pickleQty > 0) this.qtyService.pickleQty--;
  }
}
