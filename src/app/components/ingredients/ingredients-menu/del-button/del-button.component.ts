import { Component, Input, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/interfaces/ingredient/ingredient.interface';
import { EventsService } from 'src/app/services/events/events.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-del-button',
  templateUrl: './del-button.component.html',
  styleUrls: ['./del-button.component.css'],
})
export class DelButtonComponent implements OnInit {
  @Input() ingredient: Ingredient;

  constructor(
    private eventsService: EventsService
  ) {}

  ngOnInit(): void {}

  delIngredient(e: any) {
    let chosenIngredient = (e.target.innerText as string).split(' ')[1];
    switch (chosenIngredient) {
      case 'bacon':
        this.eventsService.delBaconFn.emit();
        break;
      case 'cheese':
        this.eventsService.delCheeseFn.emit();
        break;
      case 'lettuce':
        this.eventsService.delLettuceFn.emit();
        break;
      case 'meat':
        this.eventsService.delMeatFn.emit();
        break;
      case 'onion':
        this.eventsService.delOnionFn.emit();
        break;
      case 'pickle':
        this.eventsService.delPickleFn.emit();
        break;
      case 'tomato':
        this.eventsService.delTomatoFn.emit();
        break;
      default:
        console.log('No such ingredient exists!');
        break;
    }
  }

 
}
