import { Component } from '@angular/core';
import { Ingredient } from 'src/app/interfaces/ingredient/ingredient.interface';
import { IngredientsService } from 'src/app/services/ingredients/ingredients.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-ingredients-menu',
  templateUrl: './ingredients-menu.component.html',
  styleUrls: ['./ingredients-menu.component.css'],
})
export class IngredientsMenuComponent {
  ingredients: Ingredient[] = [];
  constructor(
    private ingredientService: IngredientsService,
    private qtyService: QuantitiesService
  ) {
    let self = this;
    this.ingredientService.getAllIngredients().subscribe((data) => {
      self.ingredients = data;
    });
  }
  isQtyZero(ingredientName: string) {
    switch (ingredientName) {
      case 'bacon':
        if (this.qtyService.baconQty > 0) {
          return false;
        } else {
          return true;
        }
      case 'cheese':
        if (this.qtyService.cheeseQty > 0) {
          return false;
        } else {
          return true;
        }
      case 'lettuce':
        if (this.qtyService.lettuceQty > 0) {
          return false;
        } else {
          return true;
        }
      case 'meat':
        if (this.qtyService.meatQty > 0) {
          return false;
        } else {
          return true;
        }
      case 'onion':
        if (this.qtyService.onionQty > 0) {
          return false;
        } else {
          return true;
        }
      case 'pickle':
        if (this.qtyService.pickleQty > 0) {
          return false;
        } else {
          return true;
        }
      case 'tomato':
        if (this.qtyService.tomatoQty > 0) {
          return false;
        } else {
          return true;
        }
      default:
        console.log('No such ingredient exists!');
        return false;
    }
  }
}
