import { Component, Input, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/interfaces/ingredient/ingredient.interface';
import { EventsService } from 'src/app/services/events/events.service';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.css']
})
export class AddButtonComponent implements OnInit {

  @Input() ingredient:Ingredient;
  constructor(private eventsService: EventsService) { }

  ngOnInit(): void {
  }
  addIngredient(e: any) {
    let chosenIngredient = (e.target.innerText as string).split(' ')[1];
    switch (chosenIngredient) {
      case 'bacon':
        this.eventsService.addBaconFn.emit();
        break;
      case 'cheese':
        this.eventsService.addCheeseFn.emit();
        break;
      case 'lettuce':
        this.eventsService.addLettuceFn.emit();
        break;
      case 'meat':
        this.eventsService.addMeatFn.emit();
        break;
      case 'onion':
        this.eventsService.addOnionFn.emit();
        break;
      case 'pickle':
        this.eventsService.addPickleFn.emit();
        break;
      case 'tomato':
        this.eventsService.addTomatoFn.emit();
        break;
      default:
        console.log('No such ingredient exists!');
        break;
    }
  }
}
