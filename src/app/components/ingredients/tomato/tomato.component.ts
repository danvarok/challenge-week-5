import { Component, OnInit } from '@angular/core';
import { PricesService } from 'src/app/services/prices/prices.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-tomato',
  templateUrl: './tomato.component.html',
  styleUrls: ['./tomato.component.css'],
})
export class TomatoComponent implements OnInit {
  price: number;
  constructor(private qtyService: QuantitiesService,
    private pricesService: PricesService) {
    this.price = 0.4;
    this.pricesService.tomatoPrice = this.price;
  }

  ngOnInit(): void {}

  increaseQty() {
    this.qtyService.tomatoQty++;
  }

  decreaseQty() {
    if (this.qtyService.tomatoQty > 0) this.qtyService.tomatoQty--;
  }

  getTotal =() => this.price * this.qtyService.tomatoQty;

}
