import { Component, OnInit } from '@angular/core';
import { PricesService } from 'src/app/services/prices/prices.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-meat',
  templateUrl: './meat.component.html',
  styleUrls: ['./meat.component.css'],
})
export class MeatComponent implements OnInit {
  price: number;
  constructor(
    private qtyService: QuantitiesService,
    private pricesService: PricesService
  ) {
    this.price = 1.8;
    this.pricesService.meatPrice = this.price;
  }

  ngOnInit(): void {}
  increaseQty() {
    this.qtyService.meatQty++;
  }

  decreaseQty() {
    if (this.qtyService.meatQty > 0) this.qtyService.meatQty--;
  }

}
