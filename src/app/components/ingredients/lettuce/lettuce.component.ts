import { Component, OnInit } from '@angular/core';
import { PricesService } from 'src/app/services/prices/prices.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-lettuce',
  templateUrl: './lettuce.component.html',
  styleUrls: ['./lettuce.component.css'],
})
export class LettuceComponent implements OnInit {
  price: number;
  constructor(
    private qtyService: QuantitiesService,
    private pricesService: PricesService
  ) {
    this.price = 0.4;
    this.pricesService.lettucePrice = this.price;
  }

  ngOnInit(): void {}

  increaseQty() {
    this.qtyService.lettuceQty++;
  }

  decreaseQty() {
    if (this.qtyService.lettuceQty > 0) this.qtyService.lettuceQty--;
  }

}
