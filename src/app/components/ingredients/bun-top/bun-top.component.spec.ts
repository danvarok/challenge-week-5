import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BunTopComponent } from './bun-top.component';

describe('BunTopComponent', () => {
  let component: BunTopComponent;
  let fixture: ComponentFixture<BunTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BunTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BunTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
