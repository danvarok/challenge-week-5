import { Component, OnInit } from '@angular/core';
import { PricesService } from 'src/app/services/prices/prices.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-cheese',
  templateUrl: './cheese.component.html',
  styleUrls: ['./cheese.component.css'],
})
export class CheeseComponent implements OnInit {
  price: number;
  constructor(
    private qtyService: QuantitiesService,
    private pricesService: PricesService
  ) {
    this.price = 0.6;
    this.pricesService.cheesePrice = this.price;
  }

  ngOnInit(): void {}

  increaseQty() {
    this.qtyService.cheeseQty++;
  }

  decreaseQty() {
    if (this.qtyService.cheeseQty > 0) this.qtyService.cheeseQty--;
  }

}
