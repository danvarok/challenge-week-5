import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { PricesService } from 'src/app/services/prices/prices.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-bacon',
  templateUrl: './bacon.component.html',
  styleUrls: ['./bacon.component.css'],
})
export class BaconComponent implements OnInit {
  price: number;
  constructor(private qtyService: QuantitiesService,private pricesService: PricesService) {
    this.price = 0.6;
    this.pricesService.baconPrice = this.price
  }

  ngOnInit(): void {}

  increaseQty() {
    this.qtyService.baconQty++;
  }

  decreaseQty() {
    if (this.qtyService.baconQty > 0) this.qtyService.baconQty--;
  }

}
