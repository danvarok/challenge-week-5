import { Component, OnInit } from '@angular/core';
import { PricesService } from 'src/app/services/prices/prices.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';

@Component({
  selector: 'app-onion',
  templateUrl: './onion.component.html',
  styleUrls: ['./onion.component.css']
})
export class OnionComponent implements OnInit {

  price:number;
  constructor(private qtyService:QuantitiesService,
    private pricesService: PricesService) {
    this.price = 0.40;
    this.pricesService.onionPrice = this.price;
   }

  ngOnInit(): void {
  }

  increaseQty() {
    this.qtyService.onionQty++;
  }

  decreaseQty() {
    if (this.qtyService.onionQty > 0) this.qtyService.onionQty--;
  }

}
