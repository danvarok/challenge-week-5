import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryThumbnailComponent } from './history-thumbnail.component';

describe('HistoryThumbnailComponent', () => {
  let component: HistoryThumbnailComponent;
  let fixture: ComponentFixture<HistoryThumbnailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryThumbnailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
