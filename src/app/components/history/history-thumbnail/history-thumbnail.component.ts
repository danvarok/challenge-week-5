import { componentFactoryName } from '@angular/compiler';
import {
  Component,
  ComponentFactoryResolver,
  Input,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { BurgerResponse } from 'src/app/interfaces/burger/burger-response.interface';
import { EventsService } from 'src/app/services/events/events.service';
import { QuantitiesService } from 'src/app/services/quantities/quantities.service';
import { BurgerComponent } from '../../burger/burger.component';
import { BaconComponent } from '../../ingredients/bacon/bacon.component';
import { CheeseComponent } from '../../ingredients/cheese/cheese.component';
import { LettuceComponent } from '../../ingredients/lettuce/lettuce.component';
import { MeatComponent } from '../../ingredients/meat/meat.component';
import { OnionComponent } from '../../ingredients/onion/onion.component';
import { PickleComponent } from '../../ingredients/pickle/pickle.component';
import { TomatoComponent } from '../../ingredients/tomato/tomato.component';

@Component({
  selector: 'app-history-thumbnail',
  templateUrl: './history-thumbnail.component.html',
  styleUrls: ['./history-thumbnail.component.css'],
})
export class HistoryThumbnailComponent {
  @ViewChild('componentcontainer',{ read: ViewContainerRef }) entry: ViewContainerRef;
  constructor(private resolver: ComponentFactoryResolver) {}

  @Input() burger: BurgerResponse;

  ngOnInit(): void {
    //this.burgerBuilder();
  }

  ngAfterViewInit(){
    this.burgerBuilder();
  }
  burgerBuilder() {
    this.burger.ingredients.forEach((ingredient) => {
      switch (ingredient.name.toLowerCase()) {
        case 'bacon':
          for (let i = 0; ingredient.qty > i; i++) {
            this.createBaconComponent();
          }
          break;
        case 'cheese':
          for (let i = 0; ingredient.qty > i; i++) {
            this.createCheeseComponent();
          }
          break;
        case 'lettuce':
          for (let i = 0; ingredient.qty > i; i++) {
            this.createLettuceComponent();
          }
          break;
        case 'meat':
          for (let i = 0; ingredient.qty > i; i++) {
            this.createMeatComponent();
          }
          break;
        case 'onion':
          for (let i = 0; ingredient.qty > i; i++) {
            this.createOnionComponent();
          }
          break;
        case 'pickle':
          for (let i = 0; ingredient.qty > i; i++) {
            this.createPickleComponent();
          }
          break;
        case 'tomato':
          for (let i = 0; ingredient.qty > i; i++) {
            this.createTomatoComponent();
          }
          break;
        default:
          console.log('No such ingredient exists!');
          break;
      }
    });
  }
  createBaconComponent() {
    const factory = this.resolver.resolveComponentFactory(BaconComponent);
    let newBacon = this.entry.createComponent(factory);
    this.entry.move(newBacon.hostView, 0);
  }

  createCheeseComponent() {
    const factory = this.resolver.resolveComponentFactory(CheeseComponent);
    let newCheese = this.entry.createComponent(factory);
    this.entry.move(newCheese.hostView, 0);
  }

  createLettuceComponent() {
    const factory = this.resolver.resolveComponentFactory(LettuceComponent);
    let newLettuce = this.entry.createComponent(factory);
    this.entry.move(newLettuce.hostView, 0);
  }

  createMeatComponent() {
    const factory = this.resolver.resolveComponentFactory(MeatComponent);
    let newMeat = this.entry.createComponent(factory);
    this.entry.move(newMeat.hostView, 0);
  }

  createOnionComponent() {
    const factory = this.resolver.resolveComponentFactory(OnionComponent);
    let newOnion = this.entry.createComponent(factory);
    this.entry.move(newOnion.hostView, 0);
  }

  createPickleComponent() {
    const factory = this.resolver.resolveComponentFactory(PickleComponent);
    let newPickle = this.entry.createComponent(factory);
    this.entry.move(newPickle.hostView, 0);
  }

  createTomatoComponent() {
    const factory = this.resolver.resolveComponentFactory(TomatoComponent);
    let newTomato = this.entry.createComponent(factory);
    this.entry.move(newTomato.hostView, 0);
  }
}
