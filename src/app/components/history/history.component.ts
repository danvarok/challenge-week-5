import { Component, OnInit } from '@angular/core';
import { BurgerResponse } from 'src/app/interfaces/burger/burger-response.interface';
import { OrdersService } from 'src/app/services/order/orders.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  burgerHistory:BurgerResponse[] | undefined = [];
  constructor(private orderService:OrdersService) { }

  ngOnInit(): void {
    this.orderService.getOrders().subscribe(data =>{
      this.burgerHistory = data;
    });
  }

  
}
