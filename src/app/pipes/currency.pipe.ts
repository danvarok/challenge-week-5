import { Output, Pipe,PipeTransform } from "@angular/core";
import { CurrencyService } from "../services/currency/currency.service";
@Pipe({
  name:'currencyCustom'
})
export class CurrencyPipe implements PipeTransform{
  constructor(private currencyService:CurrencyService){

  }

  transform(value:number){
    if(!value) null;
    return `${this.currencyService.currencyCode} $${value}`
  };

}