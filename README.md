# :hamburger: Burger Builder

## :computer: Usage
### Steps to run Burger Builder

- :one: Clone repository using the following command on your console.
```bash
git clone https://gitlab.com/danvarok/challenge-week-5.git
```
- :two: Once you have cloned the repository, move to the project directory and run the mock server.
```bash
npm run mock:server
```
- :three: Run te project
```bash
npm start
```
  or
```bash
ng serve
```

- :four: Open your browser and go to [localhost:4200](localhost:4200)

